#!/bin/bash
# Important: modify parameters before use. Must use absolute path.
# usage: bash rawr.runExp.sh 

# parameters
currDir=`pwd`
scriptDir="/scripts"
dataDir="/data"
taxa=10
condition=1
replicate=1
sampleNum=100
reverseRate=0.1
estimateMethod=mafft
RANDOM=$$

# setup pathways
repDataDir=${dataDir}/${taxa}taxa/condition${condition}/replicate${replicate}
cp ${repDataDir}/alignment.fasta_${estimateMethod} ${currDir}/alignment.fasta
cp ${repDataDir}/model.tree ${currDir}
cp ${repDataDir}/infer.tree ${currDir}
alnFile=${currDir}/alignment.fasta
modelTreeFile=${currDir}/model.tree
inferTreeFile=${currDir}/infer.tree
rawrSampleDir=$currDir/rawr-sample
mkdir $rawrSampleDir
treeFile=$currDir/rawr.trees

# sample trees
echo "Sample with RAWR."
cd $rawrSampleDir
python $scriptDir/sampleSeq/sampleSeq.py -a ../alignment.fasta -m RAWR -o sample -n ${sampleNum} --RAWR 0.1
for ((i=1;i<=${sampleNum};i++))
do
    mafft sample$i.seq.fasta > sample$i.aln.fasta
    raxmlHPC -f a -s sample$i.aln.fasta -n $i -m GTRGAMMA -p $RANDOM -x $RANDOM -# 10
done
cat RAxML_bestTree.* >> $treeFile
cd $currDir


