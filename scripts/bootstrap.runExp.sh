#!/bin/bash
# Important: modify parameters before use. Must use absolute path. 
# usage: bash bootstrap.runExp.sh 

# parameters
currDir=`pwd`
scriptDir="/scripts"
dataDir="/data"
taxa=10
condition=1
replicate=1
sampleNum=100
reverseRate=0.1
estimateMethod=mafft
RANDOM=$$

# setup pathways
repDataDir=${dataDir}/${taxa}taxa/condition${condition}/replicate${replicate}
cp ${repDataDir}/alignment.fasta_${estimateMethod} ${currDir}/alignment.fasta
cp ${repDataDir}/model.tree ${currDir}
cp ${repDataDir}/infer.tree ${currDir}
alnFile=${currDir}/alignment.fasta
modelTreeFile=${currDir}/model.tree
inferTreeFile=${currDir}/infer.tree
treeFile=$currDir/bootstrap.trees

# sample trees
echo "Sample with bootstrap."
raxmlHPC -s alignment.fasta -n bootstrap -m GTRGAMMA -p $RANDOM -b $RANDOM -# ${sampleNum}
mv RAxML_bootstrap.bootstrap $treeFile
