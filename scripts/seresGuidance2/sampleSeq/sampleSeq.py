#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import seqs


def validSample(sampleSeqData):
    """
    Exam the validation of sampled sequences.
    Valid sampled sequences means there is no empty sequence.

    :param sampleSeqData: pandas dataframe, sampled input alignment
                    row_index: seq id;
                    column_index: 0 to length of sampled seq.
    :return:
        True: if resampled sequences contains no empty sequences.
        False: if there is empty sequences in sampled sequences.
    """
    for i in sampleSeqData.index:
        if len(''.join(sampleSeqData.loc[i]).replace('-', '')) == 0:
            print("All empty seq:", i)
            return False
    return True


def bootstrap(alndata):
    """
    Use standard bootstrap resampling appraoch to sample input alignment.
    Write sampled sequences without gap to outfile.
    :param alndata: pandas dataframe, input alignment
            row_index: seq id
            column_index: 0 to length of input alignment
            content: a t c g and - of input alignment at each position
    :return:
        sampleIndex: sampled column indexes, column indexes of input alignment.
        sampleSeqData: sampled alignment columns
    """
    from sklearn.utils import resample
    seqnum, seqlen = alndata.shape
    isvalid = False
    while isvalid == False:
        sampleIndex = resample([x for x in range(seqlen)], n_samples=seqlen)
        sampleSeqData = alndata.iloc[:, sampleIndex]
        isvalid = validSample(sampleSeqData)
    sampleSeqData.columns = [x for x in range(sampleSeqData.shape[1])]
    return sampleIndex, sampleSeqData


def bootstrapNormal(alndata, mu, sigma):
    """
    Use bootstrap with normal distribution to sample input alignment.
    Write sampled sequences without gap to outfile.

    :param alndata: pandas dataframe, input alignment
            row_index: seq id
            column_index: 0 to length of input alignment
            content: a t c g and - of input alignment at each position
    :param mu: int, mean of the normal distribution, represents one site
    (20190520: redefeined in this function, because in IGID dataset, if mu=1888, there is never a valid relicate)
    :param sigma: int, standard deviation of the normal distribution
    :return:
        sampleIndex: sampled column indexes
        sampleSeqData: sampled alignment columns
    """

    def get_truncated_normal():
        """
        Get the sampled value with assighed mean, sd, start position and end position.
        :param mean: int, mean of the normal distribution.
        :param sd: int, standard deviation of the normal distribution.
        :param low: int, start position.
        :param upp: int, end position,
        :return: scipy.stats._distn_infrastructure.rv_frozen
        """
        from scipy.stats import truncnorm
        return truncnorm(
            (start - mu) / sigma, (end - mu) / sigma, loc=mu, scale=sigma)

    def get_sampled_index():
        """
        Get sampled column indexes.
        :param mu: int, mean of the normal distribution.
        :param sigma: int, standard deviation of the normal distribution.
        :param start: int, start position.
        :param end: int, end position.
        :param size: int, sampled size.
        :return: numpy.ndarray, sampled column indexes, int items.
        """
        x = get_truncated_normal()  # init a generator
        x = x.rvs(size=size)  # generate an array of float with assigned size
        x = x.round().astype(int)
        return x

    seqnum, seqlen = alndata.shape
    isvalid = False
    while isvalid == False:
        # mu = randint(0, seqlen - 1)
        start, end, size = 0, seqlen - 1, seqlen
        sampleIndex = get_sampled_index()
        sampleSeqData = alndata.iloc[:, sampleIndex]
        isvalid = validSample(sampleSeqData)
        print(isvalid, mu, sampleIndex)
    sampleSeqData.columns = [x for x in range(sampleSeqData.shape[1])]
    return sampleIndex.tolist(), sampleSeqData


def rawrSample(alndata, reverseRate, start=-1):
    isvalid = False
    startIndex, endIndex = alndata.columns[0], alndata.columns[-1]
    print(startIndex, endIndex)
    seqlen = alndata.shape[1]
    directionName = {1: "r", -1: "l"}
    while isvalid == False:
        if start == -1:
            currIndex = np.random.choice(seqlen, 1)[0] + startIndex
        else:
            currIndex = start
        currStart = currIndex
        direction = np.random.choice([-1, 1])  # -1: left, 1: right
        reverseList = np.random.choice(
            [-1, 1], seqlen + 1,
            p=[reverseRate,
                1 - reverseRate])  # -1 turn over, 1 stay the same direction
        sampleIndex = []
        while len(sampleIndex) < seqlen:
            if reverseList[len(sampleIndex) - 1] == -1:
                print(currStart, currIndex, directionName[direction])
                currStart = currIndex
            sampleIndex.append(currIndex)
            currIndex += direction
            if currIndex < startIndex:
                currIndex, direction = startIndex, 1
            elif currIndex > endIndex:
                currIndex, direction = endIndex, -1
            else:
                direction *= reverseList[len(sampleIndex) - 1]
        sampleSeqData = alndata.loc[:, sampleIndex]
        isvalid = validSample(sampleSeqData)

    sampleSeqData.columns = [x for x in range(sampleSeqData.shape[1])]
    return sampleIndex, sampleSeqData


def seresSample(alndata, anchorlen, anchornum, reverserate):
    seqnum, seqlen = alndata.shape
    if anchornum == -1:
        print('No specific anchor number.')
        anchornum = int(seqlen / 20) - 1
        print('Anchor number: ' + str(anchornum + 2))
    else:
        print('User set anchor number: ' + str(anchornum))
        anchornum = int(anchornum) - 2

    def distance(s1, s2):
        if len(s1) != len(s2):
            raise ValueError("Undefined for sequences of unequal length")
        l1 = np.array(list(s1))
        l2 = np.array(list(s2))

        f1 = np.invert(np.isin(l1, '-'))
        l1 = l1[f1]
        l2 = l2[f1]
        f2 = np.invert(np.isin(l2, '-'))
        l1 = l1[f2]
        l2 = l2[f2]
        return (l1 != l2).mean()

    def similarity():
        from sklearn.preprocessing import normalize
        sim = []

        def getColSimilarity(col):
            import itertools
            from collections import Counter
            totalpair = list(itertools.combinations(col, 2))
            count = Counter(totalpair)
            return sum(
                [count[x] for x in count if (x[0] == x[1] and x[0] != '-')])

        for c in alndata.columns:
            sim.append(getColSimilarity(alndata[c]))
        sim = np.array(sim)
        normsim = normalize(sim[:, None], norm='max', axis=0)
        return normsim.flatten()

    def getAnchor():
        normsim = similarity()
        seqnum, seqlen = alndata.shape
        totalScore = {
            i: sum(normsim[(i - anchorlen + 1):(i + 1)])
            for i in range(anchorlen - 1, seqlen)
        }
        anchorPool = [
            x[0] for x in sorted(
                totalScore.items(), key=lambda d: d[1], reverse=True)
        ]

        minDis = int(max(seqlen / (2 * (anchornum + 1)), anchorlen))
        barrier = [0]
        minDis = int(max(seqlen / (2 * (anchornum + 1)), anchorlen))
        for i in range(0, 0 + minDis + 1):
            if i in anchorPool:
                anchorPool.remove(i)
        for i in range(seqlen - minDis - 1, seqlen):
            if i in anchorPool:
                anchorPool.remove(i)
        for i in range(anchornum):
            bestAnchorPos = anchorPool[0]
            barrier.append(bestAnchorPos - anchorlen + 1)
            barrier.append(bestAnchorPos)
            # delete sites around selected anchor from anchorpool
            for j in range(bestAnchorPos - minDis, bestAnchorPos + minDis + 1):
                if j in anchorPool:
                    anchorPool.remove(j)
        barrier.append(seqlen - 1)
        barrier = sorted(barrier)
        return barrier

    barrier = getAnchor()
    print("Barrier index:", barrier)
    reverse = [
        np.random.choice(np.arange(2), p=[(1 - reverserate), reverserate])
        for x in range(seqlen)
    ]
    sampleValid = False

    print(
        "barrierIndex, directionIndex, startPosition, endPosition, direction")
    # while sampleValid == False:
    # bi = 0  # 20190521 find bug here, if bi=0, then the sampleValid is always false and it stuck here.
    bi = np.random.choice(np.arange(len(barrier)))
    # 20200103 find bug here, if not initial sampleIndex=[] here, the sampleIndex will increase infinitely.
    sampleIndex = []
    prevDirection = np.random.choice(np.arange(2))
    for turnover in reverse:
        if bi == 0:
            direction = 1
        elif bi == (len(barrier) - 1):
            direction = 0
        else:
            direction = abs(turnover - prevDirection)
        if bi == (len(barrier) - 1) or direction == 0:  # go backward
            temIndex = [x for x in range(barrier[bi], barrier[bi - 1], -1)]
            # print(
            #     bi, direction, barrier[bi], barrier[bi - 1], 'l', sep=', ')
            bi -= 1
        elif bi == 0 or direction == 1:
            temIndex = [x for x in range(barrier[bi], barrier[bi + 1])]
            # print(
            #     bi, direction, barrier[bi], barrier[bi + 1], 'r', sep=', ')
            bi += 1
        sampleIndex.extend(temIndex)
        prevDirection = direction
        if len(sampleIndex) >= seqlen:
            break
    sampleSeqData = alndata.iloc[:, sampleIndex]
    # sampleValid = validSample(sampleSeqData)

    sampleSeqData.columns = [x for x in range(sampleSeqData.shape[1])]
    return sampleIndex, sampleSeqData


def sampleAndGetSampleAlnData(alndata, samplenum, sampleMethod, mu):
    """
    Use Bootstrap with normal distribution to sample sequences from the original alignment
    Align sequences and calculate the support value for sampled sites.
    :param alndata: pandas dataframe, input alignment
            row_index: seq id
            column_index: 0 to length of input alignment
            content: a t c g and - of input alignment at each position
    :param mu: int, mean of the normal distribution.
    :param samplenum: the index of sampled file.
    :return: defaultdict(int), support value for column index correspond to input alignment.
    """
    import subprocess
    # sample seq data
    if sampleMethod == 'bootstrapNormal':
        sampleIndex, sampleSeqData = bootstrapNormal(alndata, mu, 100)
    elif sampleMethod == 'bootstrap':
        sampleIndex, sampleSeqData = bootstrap(alndata)
    elif sampleMethod == 'RAWR':
        window = alndata.loc[:,
                             max(0, mu - 100):min(alndata.shape[1], mu + 100)]
        reverserate = 0.1
        sampleIndex, sampleSeqData = rawrSample(
            window, reverserate, mu)
    print("sampleIndex:", sampleIndex)
    # write seq data to outfile
    sampleIdxFile = 'sample' + str(samplenum) + '.index'
    sampleSeqFile = 'sample' + str(samplenum) + '.seq.fasta'
    sampleAlnFile = 'sample' + str(samplenum) + '.aln.fasta'
    with open(sampleSeqFile, 'w') as outf:
        for i in sampleSeqData.index:
            outf.write('>' + sampleSeqData.loc[i].name + '\n')
            outf.write(''.join(sampleSeqData.loc[i]).replace('-', '') + '\n')
    # with open(sampleIdxFile, 'w') as outf:
    #     outstr = ','.join([str(x) for x in sampleIndex])
    #     outf.write(outstr + '\n')
    np.save(sampleIdxFile, np.array(sampleIndex))
    # generate alignment
    cmd = 'mafft ' + sampleSeqFile + ' > ' + sampleAlnFile
    with open("NUL", "w") as fh:
        p = subprocess.Popen(cmd, shell=True, stdout=fh, stderr=fh)
        output = p.communicate()[0]
    sampleAlnData = seqs.getAlnData(sampleAlnFile)
    return sampleIndex, sampleSeqData, sampleAlnData


def plotIndex(sampleIndex):
    import matplotlib.pyplot as plt
    from collections import Counter
    c = Counter(sampleIndex)
    plt.plot([x for x in sorted(c)], [c[x] for x in sorted(c)])
    plt.show()


def main():
    import argparse
    import seqs

    parser = argparse.ArgumentParser(
        description='Parameters for sampling sequences.')
    parser.add_argument(
        '--alnfile',
        '-a',
        help='fasta file of input alignment.',
        required=True)
    parser.add_argument(
        '--method',
        '-m',
        help='sample method. bootstrap, bootstrapNormal, seres, RAWR.',
        required=True)
    parser.add_argument(
        '--outfilename',
        '-o',
        help='outfile for sampled sequences.',
        required=True)
    parser.add_argument(
        '--samplenum',
        '-n',
        help='number for sampled sequences.',
        required=True)
    parser.add_argument(
        '--bootstrapNormal',
        '-bn',
        help='bootstrap normal parameters, mu and sigma',
        nargs='*')
    parser.add_argument(
        '--seres',
        '-s',
        help='seres parameters, anchorlen, anchornum, and reverseRate',
        nargs='*')
    parser.add_argument(
        '--RAWR', '-sn', help='RAWR parameters, reverseRate')
    args = parser.parse_args()

    alnfile = args.alnfile
    outfile = args.outfilename
    samplenum = int(args.samplenum)
    sampleMethod = args.method
    alndata = seqs.getAlnData(alnfile)
    for n in range(1, samplenum + 1):
        if sampleMethod == 'bootstrap':
            sampleIndex, sampleSeqData = bootstrap(alndata)
        elif sampleMethod == 'bootstrapNormal':
            mu, sigma = args.bootstrapNormal
            sampleIndex, sampleSeqData = bootstrapNormal(
                alndata, int(mu), int(sigma))
        elif sampleMethod == 'RAWR':
            reverseRate = args.RAWR
            sampleIndex, sampleSeqData = rawrSample(
                alndata, float(reverseRate))
        elif sampleMethod == 'seres':
            anchorLen, anchorNum, reverseRate = args.seres
            sampleIndex, sampleSeqData = seresSample(alndata, int(anchorLen),
                                                     int(anchorNum),
                                                     float(reverseRate))
        else:
            print(
                'Please assign a sampling method: bootstrap, bootstrapNormal, RAWR, seres'
            )
        with open(outfile + str(n) + ".seq.fasta", 'w') as outf:
            for i in sampleSeqData.index:
                outf.write('>' + sampleSeqData.loc[i].name + '\n')
                outf.write(''.join(sampleSeqData.loc[i]).replace('-', '') +
                           '\n')
        np.save(outfile + str(n) + ".index", np.array(sampleIndex))


if __name__ == "__main__":
    main()
