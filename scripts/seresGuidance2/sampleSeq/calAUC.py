from sklearn.metrics import precision_recall_curve, roc_auc_score, roc_curve, auc

def prauc(truth, pred):
    precision, recall, thresholds = precision_recall_curve(truth, pred)
    pr = auc(recall, precision)
    return precision, recall, pr

def rocauc(truth, pred):
    fpr, tpr, threshold = roc_curve(truth, pred)
    roc = auc(fpr, tpr)
    return fpr, tpr, roc
