#!/usr/bin/python


def checkFileExist(alignmentFile, outDir):
    """Check if alignment file and output directory exist.

    Args:
        infile: The path of input alignment file.
        outDir: The path of output directory.

    Raises:
        If file or output directory does not exist, program ends.
    """

    import sys
    import os
    fileExist = os.path.exists(alignmentFile.strip())
    outDirExist = os.path.isdir(outDir.strip())
    if fileExist == False:
        sys.exit("Alignment file does not exist.")
    if outDirExist == False:
        sys.exit("Output diractory does not exist.")


def produceSeqFile(alignment, outDir):
    """Convert user alignment file to sequence file.
    Remove gaps in alignments, write sequences to output directory with original ID.

    Args:
        alignmentFile: The path of user alignment file.
        outDir: The path of output directory.

    OutPut:
        seqFile: outDir/UserSeqOrig, sequences produced through removing gaps in user alignments. ID are the same as alignments provided.
    """

    if outDir[-1] != "/":
        seqFile = outDir + "/UserSeq_guidance2"
    else:
        seqFile = outDir + "UserSeq_guidance2"
    seqF = open(seqFile, 'wt')
    for i in alignment:
        seqF.write('>' + str(i.id) + '\n')
        seqF.write(str(i.seq).replace('-', '') + '\n')
    seqF.close()
    print("Fixed user sequence file: "+str(seqFile))

def renameUserAlignmentID(alignmentFile, outDir):
    """Rename user alignments with new id.
    The user alignments are renamed with the order of sequences. The fixed UserMSA file is the same as UserMSA.FIXED produced by GUIDANCE2. The match between original ID and fixed ID saved in idMatch.

    Args:
        alignmentFile: The path of user alignment file.
        outDir: The path of output directory.
    """
    from Bio import AlignIO
    import simplejson as json

    origAln = AlignIO.read(alignmentFile, "fasta")
    if outDir[-1] != "/":
        fixedAlnFile = outDir + "/FixedUserMSA_guidance2"
    else:
        fixedAlnFile = outDir + "FixedUserMSA_guidance2"
    fixedAlnF = open(fixedAlnFile, 'wt')
    idMatch = {}
    for i in range(len(origAln)):
        temaln = origAln[i]
        fixedID = "seq" + str("%04d" % i)
        idMatch[str(temaln.id)] = fixedID
        fixedAlnF.write('>' + str(fixedID) + '\n')
        fixedAlnF.write(str(temaln.seq).upper() + '\n')
    fixedAlnF.close()
    print("Fixed user alignment file: "+str(fixedAlnFile))
    if outDir[-1] != "/":
        idMatchFile = outDir + "/idMatch_guidance2"
    else:
        idMatchFile = outDir + "idMatch_guidance2"
    with open(idMatchFile, 'wt') as outf:
        json.dump(idMatch, outf)
    print("Match of original sequence id and fixed id file: "+str(idMatchFile))

def preprocessAln(alignment,filename):
    with open(filename, 'w') as outf:
        for i in alignment:
            if str(i.seq).replace('-','')!='':
                outf.write('>' + str(i.id) + '\n')
                refinedseq=str(i.seq).replace('u', 't')
                refinedseq=refinedseq.replace('U', 'T')
                refinedseq=refinedseq.upper()
                ntList=['A','T','C','G','-']
                for ch in refinedseq:
                    if ch not in ntList:
                        refinedseq=refinedseq.replace(ch, '-')
                outf.write(refinedseq + '\n')

import sys
from Bio import AlignIO
alignmentFile = sys.argv[1]
outDir = sys.argv[2]

checkFileExist(alignmentFile, outDir)
alignment = AlignIO.read(alignmentFile, "fasta")
if outDir[-1] != "/":
    refinedAlnFile = outDir + "/refinedAlignment.fasta"
else:
    refinedAlnFile = outDir + "refinedAlignment.fasta"
preprocessAln(alignment, refinedAlnFile)
renameUserAlignmentID(refinedAlnFile, outDir)
print("Produce user alignments with fixed alignment id.")
if outDir[-1] != "/":
    fixedAlnFile = outDir + "/FixedUserMSA_guidance2"
else:
    fixedAlnFile = outDir + "FixedUserMSA_guidance2"
alignment = AlignIO.read(fixedAlnFile, "fasta")
produceSeqFile(alignment, outDir)
print('Finished preprocessing')
