#!/bin/bash
# Important: modify parameters before use. Must use absolute path.
# usage: bash seresGuidance2.runExp.sh 

# parameters
currDir=`pwd`
guidancedir="/Software/guidance.v2.02"
scriptDir="/scripts/seresGuidance2"
dataDir="/data"
taxa=10
condition=1
replicate=1
sampleNum=100
reverseRate=0.1
estimateMethod=mafft

# setup pathways
RANDOM=$$
repDataDir=${dataDir}/${taxa}taxa/condition${condition}/replicate${replicate}
cp ${repDataDir}/alignment.fasta_${estimateMethod} ${currDir}/alignment.fasta
cp ${repDataDir}/model.tree ${currDir}
cp ${repDataDir}/infer.tree ${currDir}
alnFile=${currDir}/alignment.fasta
modelTreeFile=${currDir}/model.tree
inferTreeFile=${currDir}/infer.tree
guidanceSampleDir=${currDir}/guidance2-sample
guidanceOutDir=${guidanceSampleDir}/guidance2-results
altermsadir=${guidanceSampleDir}/guidance-alterMSA/
treeFile=${currDir}/seresGuidance2.trees
fixedMSAfile=${guidanceSampleDir}/FixedUserMSA_guidance2
logFile=${guidanceOutDir}/BP/MSA.MAFFT.semphy.log
mkdir ${guidanceSampleDir}
mkdir ${guidanceOutDir}
mkdir ${guidanceOutDir}/BP/
mkdir ${altermsadir}

# seres resampling
echo "Sample with SERES+GUIDANCE2."
cd ${guidanceSampleDir}
python ${scriptDir}/preprocessing.py ${alnFile} ${guidanceSampleDir}
python ${scriptDir}/sampleSeq/sampleSeq.py -a ${fixedMSAfile} -m seres -o sample -n 101 --seres 5 -1 ${reverseRate}
for ((i=1;i<=101;i++))
do
    mafft sample${i}.seq.fasta > sample_aln${i}.fasta
    ${guidancedir}/programs/semphy/semphy -a 4 --hky -J -H -v 8 -s sample_aln${i}.fasta -o MSA.MAFFT.semphy.out -T sample_${i}.tree -l MSA.MAFFT.semphy.log >> MSA.MAFFT.semphy.std
done
rm MSA.MAFFT.semphy.out
rm MSA.MAFFT.semphy.log
rm MSA.MAFFT.semphy.std

# create log file with resampled trees
echo 'START OF LOG FILE' > ${logFile}
echo ` cat sample_1.tree ` >> ${logFile}
echo '# distanceBasedSeqs2Tree::seqs2Tree: The reconsructed tree: ' >> ${logFile}
echo ` cat sample_1.tree ` >> ${logFile}
echo '# Finished tree reconstruction.' >> ${logFile}
echo '# The log likelihood of the tree is:' >> ${logFile}
echo '# The tree' >> ${logFile}
echo ` cat sample_1.tree ` >> ${logFile}
for ((i=2;i<=101;i++))
do
echo ` cat sample_${i}.tree ` >> ${logFile}
echo '# distanceBasedSeqs2Tree::seqs2Tree: The reconsructed tree:' >> ${logFile}
echo ` cat sample_${i}.tree ` >> ${logFile}
done
echo '# The bootstrap values imposed on the resulting tree:' >> ${logFile}
echo 'END OF LOG FILE' >> ${logFile}

# move modified guidance script with no bootstrap step to guidance dir and run guidance2
cp ${scriptDir}/guidance2_UserBP.pl ${guidancedir}/www/Guidance/
cd ${guidancedir}/www/Guidance/
module load BioPerl
perl ${guidancedir}/www/Guidance/guidance2_UserBP.pl --seqFile ${guidanceSampleDir}/UserSeq_guidance2 --msaProgram MAFFT --seqType nuc --outDir ${guidanceOutDir} --program GUIDANCE2 --bootstraps 100 --msaFile ${fixedMSAfile}

# get alterMSA
cp ${guidanceOutDir}/MSA.MAFFT.Guidance2_AlternativeMSA.tar.gz ${altermsadir}
cd ${altermsadir}
tar -zxvf MSA.MAFFT.Guidance2_AlternativeMSA.tar.gz
for f in `ls b*.fasta|head -n ${sampleNum}`
do
    raxmlHPC -s ${f} -n ${f} -m GTRGAMMA -p ${RANDOM} -# 10
done
cat RAxML_bestTree.* > ${guidanceSampleDir}/guidance2.trees_filxedID
cd ${guidanceSampleDir}
python $scriptDir/matchid.py 
mv guidance2.trees ${treeFile}
cd ${currDir}

