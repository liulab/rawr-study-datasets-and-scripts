#!/usr/bin/python
import simplejson as json
import sys
from sklearn.metrics import roc_curve, auc, roc_auc_score
from sklearn.metrics import precision_recall_curve
import re


def removeBranchLen(tree):
    numList = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".", ":"]
    flag = 0
    temtree = ""
    for i in tree.strip():
        if i == ":":
            flag = 1
        if (flag == 1) and (i not in numList):
            flag = 0
        if flag == 0:
            temtree += i
    return temtree


def treeNodes(tree):
    cleanTree = tree.replace('(', '').replace(')', '').replace(';', '')
    # remove "(", ")", ";"
    cleanTree = re.sub(r'\[+.*?\]+', '', cleanTree)
    # remove [***]
    return cleanTree.strip().split(',')


def findBiPartitons(tree, withSupport):
    allNodes = treeNodes(tree)
    bipartition = {}

    for i, c in enumerate(tree):
        if c == ')' and tree[i + 1] != ';':
            # find bipartitions
            rawnodes = ''
            stack = list(tree[:i])
            needed = 1
            while stack:
                if stack[-1] == ')':
                    needed += 1
                elif stack[-1] == '(':
                    needed -= 1
                if needed == 0:
                    break
                rawnodes += stack.pop()
            rawnodes = rawnodes[::-1]
            b1 = treeNodes(rawnodes)
            b2 = [x for x in allNodes if x not in b1]

            support = -1
            if withSupport and tree[i + 1] == '[':
                # find support
                support = ''
                temindex = i + 2
                while temindex < len(tree) and tree[temindex] != ']':
                    support += tree[temindex]
                    temindex += 1
                if support.isdigit():
                    support = float(support) / 100.

            b1.sort()
            b2.sort()
            b1str = ','.join(b1)
            b2str = ','.join(b2)
            bi = b1str + '|' + b2str
            bipartition[bi] = support
    return bipartition


def readTree(infile):
    with open(infile, 'r') as inf:
        for line in inf.readlines():
            if line[0] == '(':
                tree = line.strip()
    return tree


modeltreefile = sys.argv[1]
supptreefile = sys.argv[2]

modeltree = readTree(modeltreefile)
supp = readTree(supptreefile)
modeltree = removeBranchLen(modeltree)
supp = removeBranchLen(supp)
modelbi = findBiPartitons(modeltree, 0)
suppbi = findBiPartitons(supp, 1)

truth = []
pred = []

for b in suppbi:
    b1 = b
    b2 = '|'.join(b.split('|')[::-1])
    if b1 in modelbi or b2 in modelbi:
        truth.append(1)
    else:
        truth.append(0)
    pred.append(suppbi[b])

with open('truth', 'w') as acl:
    json.dump(truth, acl)
with open('prediction', 'w') as acl:
    json.dump(pred, acl)

fpr, tpr, threshold = roc_curve(truth, pred)
roc_auc = auc(fpr, tpr)

with open('fpr', 'w') as outf:
    for i in fpr:
        outf.write(str(i) + '\n')
with open('tpr', 'w') as outf:
    for i in tpr:
        outf.write(str(i) + '\n')
with open('rocauc', 'w') as outf:
    outf.write(str(roc_auc) + '\n')
print("Area Under Curve for ROC Curve: " + str(roc_auc))

precision, recall, thresholds = precision_recall_curve(truth, pred)
roc_auc = auc(recall, precision)

with open('precision', 'w') as outf:
    for i in precision:
        outf.write(str(i) + '\n')
with open('recall', 'w') as outf:
    for i in recall:
        outf.write(str(i) + '\n')
with open('prauc', 'w') as outf:
    outf.write(str(roc_auc) + '\n')
print("Area Under Curve for Precision-Recall Curve: " + str(roc_auc))
