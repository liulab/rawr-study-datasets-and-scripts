#!/bin/bash
# Important: modify parameters before use. Must use absolute path.
# usage: bash guidance2.runExp.sh 

# parameters
currDir=`pwd`
guidancedir="/Software/guidance.v2.02"
scriptDir="/scripts"
dataDir="/data"
taxa=10
condition=1
replicate=1
sampleNum=100
reverseRate=0.1
estimateMethod=mafft
RANDOM=$$

# setup pathways
repDataDir=${dataDir}/${taxa}taxa/condition${condition}/replicate${replicate}
cp ${repDataDir}/sequence.fasta ${currDir}
cp ${repDataDir}/alignment.fasta_${estimateMethod} ${currDir}/alignment.fasta
cp ${repDataDir}/model.tree ${currDir}
cp ${repDataDir}/infer.tree ${currDir}
seqFile=${currDir}/sequence.fasta
alnFile=${currDir}/alignment.fasta
modelTreeFile=${currDir}/model.tree
inferTreeFile=${currDir}/infer.tree
guidanceSampleDir=${currDir}/guidance2-sample
guidanceOutDir=${guidanceSampleDir}/guidance2-results
altermsadir=${guidanceSampleDir}/guidance-alterMSA/
mkdir ${guidanceSampleDir}
mkdir ${guidanceOutDir}
mkdir ${altermsadir}
treeFile=${currDir}/guidance2.trees

# sample trees
echo "Sample with GUIDANCE2."
cd ${guidancedir}/www/Guidance/
module load BioPerl
perl ${guidancedir}/www/Guidance/guidance.pl --seqFile ${seqFile}  --msaProgram MAFFT --seqType nuc --outDir ${guidanceOutDir} --program GUIDANCE2 --bootstraps 100 --msaFile ${alnFile}
cd ${currDir}
cp ${guidanceOutDir}/MSA.MAFFT.Guidance2_AlternativeMSA.tar.gz ${altermsadir}
cd ${altermsadir}
tar -zxvf MSA.MAFFT.Guidance2_AlternativeMSA.tar.gz
for f in `ls b*.fasta|head -n ${sampleNum}`
do
    raxmlHPC -s ${f} -n ${f} -m GTRGAMMA -p ${RANDOM} -# 10
done
cat RAxML_bestTree.* > ${treeFile}
cd ${currDir}


