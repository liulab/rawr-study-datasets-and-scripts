#!/bin/bash
repDir=`pwd`
RANDOM=$$
scriptDir="/mnt/gs18/scratch/users/wangwe90/phyloSupport.seresNoAnchor.20200108/simulation.runningTimeAndMemory/guidance2/scripts"

cd /mnt/home/wangwe90/Software/guidance.v2.02/www/Guidance/
module load BioPerl
echo "perl /mnt/home/wangwe90/Software/guidance.v2.02/www/Guidance/guidance.pl --seqFile $repDir/sequence.fasta  --msaProgram MAFFT --seqType nuc --outDir $repDir/guidance2/ --program GUIDANCE2 --bootstraps 100 --msaFile $repDir/alignment.fasta"
perl /mnt/home/wangwe90/Software/guidance.v2.02/www/Guidance/guidance.pl --seqFile $repDir/sequence.fasta  --msaProgram MAFFT --seqType nuc --outDir $repDir/guidance2/ --program GUIDANCE2 --bootstraps 100 --msaFile $repDir/alignment.fasta

cd $repDir
mkdir guidance_alterMSA/
cp guidance2/MSA.MAFFT.Guidance2_AlternativeMSA.tar.gz guidance_alterMSA/
cd guidance_alterMSA/
tar -zxvf MSA.MAFFT.Guidance2_AlternativeMSA.tar.gz
start=`date +%s`
for i in `ls *.fasta|head -n 10`
do
    raxmlHPC -s $i -n $i -m GTRGAMMA -p $RANDOM -# 10
    mv RAxML_bestTree.$i ${i}.tree
done
end=`date +%s`
runtime=$((end-start))
echo "MSA and tree estimation running time: $runtime"

rm RAxML_*
cat *.tree > $repDir/guidance2.trees.100
cd $repDir
tar -czvf guidance2.tar.gz guidance2
rm -r guidance2
tar -czvf guidance_alterMSA.tar.gz guidance_alterMSA
rm -r guidance_alterMSA
# calculate support value for inferTree
raxmlHPC -f b -m GTRGAMMA -t infer.tree -z guidance2.trees.100 -n infer-guidance2.100
# calculate AUC for support value against model tree
python $scriptDir/calAUC.py model.tree RAxML_bipartitionsBranchLabels.infer-guidance2.100 infer-guidance2.100


