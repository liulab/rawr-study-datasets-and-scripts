1. Empirical dataset of the RAWR study
2. Wei Wang, the Department of Computer Science and Engineering, Michigan State University, wangwe90@msu.edu
3. All estimated alignments were generated from raw reads originally downloaded from the NCBI Sequence Read Archive (SRA) database (https://www.ncbi.nlm.nih.gov/bioproject/PRJNA263122/).
4. This empirical dataset contains 30207 estimated alignment files. 
5. This folder contains following files:
- filteredAlns.file.txt: list all the estimated alignment files packed in filteredAlns.tar.gz.
- filteredAlns.tar.gz: compressed file of all the estimated alignment files. 
- readme.txt: description of this empirical dataset. 
- sra.txt: name list of sampled individuals. 

-added by Meijun
There are 30207 alignemnt in Wei's filteredAlns, we can concatenate all 30207 alignments into a concatenated large MSA
but there are some alignments within the 30207 that do not all have 4 types of nucletides (it will lead to collapse of RAXML),
hence we delete these alignments in the concatenated one, and get the new concatenated file and its partition file in ConcatAlns_Meijun.
There are 30176 alignments.

-added by Preethi
262 alignment files out of 30207 files in Wei's filteredAlns are empty and should be discarded.



